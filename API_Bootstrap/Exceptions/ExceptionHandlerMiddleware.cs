﻿using API_Bootstrap.Exposition.Exceptions;
using API_Bootstrap.Exposition.Interfaces;

namespace API_Bootstrap.Exceptions
{
    public class ExceptionHandlerMiddleware : IMiddleware
    {
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;

        public ExceptionHandlerMiddleware(ILogger<ExceptionHandlerMiddleware> logger)
        {
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            EventId exceptionId = new(0, Guid.NewGuid().ToString());
            var (statusCode, message) = (0, string.Empty);
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                switch (exception)
                {
                    case IInformationsException:
                        _logger.LogInformation(exceptionId, exception, exception.Message, exception.Data);
                        break;
                    case IErrorException:
                        _logger.LogError(exceptionId, exception, exception.Message, exception.Data);
                        break;
                    case IWarningException:
                        _logger.LogWarning(exceptionId, exception, exception.Message, exception.Data);
                        break;
                    case ICriticalException:
                    default:
                        _logger.LogCritical(exceptionId, exception, exception.Message, exception.Data);
                        break;
                }

                if (exception is ApiSafeException apiSafeException)
                    (statusCode, message) = ((int)apiSafeException.StatusCode, apiSafeException.SafeErrorMessage);

                else if (exception is IApiException apiException)
                    (statusCode, message) = ((int)apiException.StatusCode, "Erreur interne connue, veuillez réessayer et, si le problème persiste, consulter le service IT");

                else
                    (statusCode, message) = (500, $"Erreur interne inconnue, veuillez réessayer et, si le problème persiste, consulter le service IT (code erreur {exceptionId.Name})");

                context.Response.StatusCode = statusCode;
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsJsonAsync(message);
            }
        }
    }
}
