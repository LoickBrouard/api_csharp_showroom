﻿using System.Net;

using API_Bootstrap.Exposition.Exceptions;
using API_Bootstrap.Exposition.Interfaces;

namespace API_Bootstrap.Exceptions
{
    public class InputException : ApiSafeException, IErrorException
    {
        public InputException(string inputField) : base($"input missing or incorrect :  {inputField}") { }

        public InputException(string inputField, Exception innerException) : base($"input missing or incorrect : {inputField}", innerException) { }

        public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
    }
}
