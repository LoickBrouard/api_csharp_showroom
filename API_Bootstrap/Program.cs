using API_Bootstrap.Configurations;
using API_Bootstrap.Exceptions;
using API_Bootstrap.Handlers;

using InputAndDispatcher.Handlers;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddControllers()
    .AddJsonOptions(options => options.AddApiJsonConfiguration());

builder.Services
    .AddEndpointsApiExplorer()
    .AdExceptionHandlerMiddleware()
    .AddDatababaseServices()
    .AddSwaggerGen(options => options.ApiSwaggerOptions())
    .AddDispatchers() // From the InputAndDispatcher NuGet
    .AddApiHandlers() // From the Handlers directory
    .AddNlogService(builder.Configuration.GetConnectionString("LogConnection"));

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
    app.UseDeveloperExceptionPage();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<ExceptionHandlerMiddleware>();

app.Run();
