﻿using API_Bootstrap.Exposition.Input;

using InputAndDispatcher.Common.Interfaces;

namespace API_Bootstrap.Handlers
{
    public static class ServicesRegistration
    {
        public static IServiceCollection AddApiHandlers(this IServiceCollection services)
        {
            return services
                .AddScoped<ICommandHandler<DummyCommand, int>, DummyCommandHandler>()
                .AddScoped<IQueryHandler<DummyQuery, int?>, DummyQuerryHandler>();
        }
    }
}
