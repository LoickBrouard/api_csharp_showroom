﻿using API_Bootstrap.Exposition.Input;

using InputAndDispatcher.Common.Interfaces;

namespace API_Bootstrap.Handlers
{
    public sealed class DummyCommandHandler : ICommandHandler<DummyCommand, int>
    {
        public DummyCommandHandler()
        {
            // DI and init
        }

        /// <inheritdoc/>
        public int Handle(DummyCommand command)
        {
            // Do interresting thing
            return 0;
        }
    }
}
