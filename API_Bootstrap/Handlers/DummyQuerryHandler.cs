﻿using API_Bootstrap.Exposition.Input;

using InputAndDispatcher.Common.Interfaces;

namespace API_Bootstrap.Handlers
{
    public sealed class DummyQuerryHandler : IQueryHandler<DummyQuery, int?>
    {
        public DummyQuerryHandler()
        {
            // DI and init
        }

        public int? Handle(DummyQuery query)
        {
            // Do interesting things
            bool lucky = true;
            if (lucky)
                return 777;

            return null;
        }
    }
}
