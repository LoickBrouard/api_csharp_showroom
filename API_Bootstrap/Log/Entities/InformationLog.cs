﻿namespace API_Bootstrap.Log.Entities
{
    public class InformationLog
    {
        public int Id { get; set; }
        public string EventId { get; set; }
        public DateTime Date { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string MachineName { get; set; }
    }
}
