﻿using API_Bootstrap.Log.Entities;

using Microsoft.EntityFrameworkCore;

namespace API_Bootstrap.Log;

public sealed class ApiLogDbContext : DbContext
{
    public ApiLogDbContext() { }
    public ApiLogDbContext(DbContextOptions<ApiLogDbContext> options) : base(options) { }

    public DbSet<InformationLog> InformationLog { get; set; }
    public DbSet<ErrorLog> ErrorLog { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}