using API_Bootstrap.Exposition.Input;
using API_Bootstrap.Exposition.Output;

using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Common.Models;

using Microsoft.AspNetCore.Mvc;

namespace API_Bootstrap.Controllers
{

    [ApiController]
    [Route("[Controller]/[Action]")]
    public class DummyController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ILogger<DummyController> _logger;

        public DummyController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ILogger<DummyController> logger)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
            _logger = logger;
        }

        #region GET_METHODS

        /// <summary>
        /// To get things from DB
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<DummyOutput> GetAssociationLock([FromQuery] DummyQuery query)
        {
            if (query.Equals(null))
                return BadRequest("No parameters has been sent to the API");

            InputValidationResult validation = query.Validate();

            if (validation.IsFailure)
                return BadRequest(string.Join(",", validation.ErrorMessages));

            if (_queryDispatcher.Dispatch<DummyQuery, int?>(query) == 42)
                return Ok(new DummyOutput() { ResultValue = "Trouv� 42!" });

            return Ok(new DummyOutput() { ResultValue = "Trouv� autre chose !" });
        }

        #endregion

        #region POST_METHODS

        /// <summary>
        /// To create thing in db
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<int> AssociatePanelAndLock(DummyCommand command)
        {
            if (command.Equals(null))
                return BadRequest("No parameters has been sent to the API");

            InputValidationResult validation = command.Validate();

            if (validation.IsFailure)
                return BadRequest(string.Join(",", validation.ErrorMessages));

            return Ok(_commandDispatcher.Dispatch<DummyCommand, int>(command));
        }

        #endregion
    }
}