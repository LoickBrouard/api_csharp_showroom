﻿using API_Bootstrap.Log;

using Microsoft.EntityFrameworkCore;

namespace API_Bootstrap.Configurations
{
    public static class EntityFrameworkConfiguration
    {
        public static IServiceCollection AddDatababaseServices(this IServiceCollection services)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environmentName}.json", true, true)
                .AddEnvironmentVariables();
            var configuration = builder.Build();

            return services.AddDbContext<ApiLogDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("LogConnection"))
            );
        }
    }
}
