﻿using NLog;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Targets;

namespace API_Bootstrap.Configurations
{
    public static class NlogConfiguration
    {
        public static IServiceCollection AddNlogService(this IServiceCollection services, string connectionString)
        {
            LoggingConfiguration loggingConfiguration = new() { };

            // Création des targets
            DatabaseTarget errorDatabaseTarget = CreateErrorDatabaseTarget(connectionString);
            DatabaseTarget informationDatabaseTarget = CreateInformationDatabaseTarget(connectionString);
            ConsoleTarget consoleTarget = new()
            {
                AutoFlush = true,
                Layout = "${event-properties:EventName}|${longdate}|${level}|${message} |${all-event-properties} ${exception:format=tostring}",
                Name = "logconsole"
            };

            FileTarget informationFile = new("InformationFile")
            {
                ArchiveEvery = FileArchivePeriod.Day,
                ArchiveNumbering = ArchiveNumberingMode.DateAndSequence,
                FileName = "C:\\Codes_Logs${dir-separator}${appdomain:format={1\\}}${dir-separator}${shortdate}_UserWindowsService_Infos.log",
                Layout = "${event-properties:EventName}|${longdate}|${level}|${message} |${all-event-properties} ${exception:format=tostring}",
                ArchiveFileName = "{###}Arch_UserWindowsService_Infos.log",
            };

            FileTarget errorFile = new("ErrorFile")
            {
                ArchiveEvery = FileArchivePeriod.Day,
                ArchiveNumbering = ArchiveNumberingMode.DateAndSequence,
                FileName = "C:\\Codes_Logs${dir-separator}${appdomain:format={1\\}}${dir-separator}${shortdate}_UserWindowsService_Error.log",
                Layout = "${event-properties:EventName}|${longdate}|${level}|${message} |${all-event-properties} ${exception:format=tostring}",
                ArchiveFileName = "{###}Arch_UserWindowsService_Error.log",
            };

            // Ajout des targets
            loggingConfiguration.AddTarget("InformationFile", informationFile);
            loggingConfiguration.AddTarget("ErrorFile", errorFile);
            loggingConfiguration.AddTarget("ApiErrorLog", errorDatabaseTarget);
            loggingConfiguration.AddTarget("ApiLog", informationDatabaseTarget);
            loggingConfiguration.AddTarget("ConsoleLog", consoleTarget);

            // Configuration des règles d'attributions
            loggingConfiguration.LoggingRules.Add(new("System.*") { FinalMinLevel = NLog.LogLevel.Warn });
            loggingConfiguration.LoggingRules.Add(new("Microsoft.*") { FinalMinLevel = NLog.LogLevel.Warn });
            loggingConfiguration.LoggingRules.Add(new("Microsoft.Hosting.Lifetime*") { FinalMinLevel = NLog.LogLevel.Warn });

            loggingConfiguration.LoggingRules.Add(new("*", NLog.LogLevel.Trace, consoleTarget));
            loggingConfiguration.LoggingRules.Add(new("*", NLog.LogLevel.Trace, NLog.LogLevel.Warn, informationFile));

            loggingConfiguration.LoggingRules.Add(new("*", NLog.LogLevel.Error, errorDatabaseTarget));
            loggingConfiguration.LoggingRules.Add(new("*", NLog.LogLevel.Error, errorFile));

            loggingConfiguration.LoggingRules.Add(new($"{nameof(API_Bootstrap)}.*", NLog.LogLevel.Info, NLog.LogLevel.Info, informationDatabaseTarget));


            LogManager.Configuration = loggingConfiguration;

            return services.AddLogging(loggingBuilder =>
            {
                // configure Logging with NLog
                loggingBuilder.ClearProviders();
                loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                loggingBuilder.AddNLog(loggingConfiguration, new NLogProviderOptions { IgnoreEmptyEventId = false });
            });
        }
        static DatabaseTarget CreateErrorDatabaseTarget(string connectionString)
        {
            DatabaseTarget dbTarget = new()
            {
                ConnectionString = connectionString,
                CommandText = @"INSERT INTO [dbo].[ErrorLog]
				([EventId], [Date], [Level], [Logger], [Message], [StackTrace], [MachineName])
				VALUES
				(@EventId, @Date, @Level, @Logger, @Message, @StackTrace, @MachineName)",

            };
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@EventId", new NLog.Layouts.SimpleLayout("${event-properties:EventName}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Date", new NLog.Layouts.SimpleLayout("${longdate}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Level", new NLog.Layouts.SimpleLayout("${level}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Logger", new NLog.Layouts.SimpleLayout("${logger}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Message", new NLog.Layouts.SimpleLayout("${message}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@StackTrace", new NLog.Layouts.SimpleLayout("${exception:tostring}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@MachineName", new NLog.Layouts.SimpleLayout("${machinename}")));

            return dbTarget;
        }

        static DatabaseTarget CreateInformationDatabaseTarget(string connectionString)
        {
            DatabaseTarget dbTarget = new()
            {
                ConnectionString = connectionString,
                CommandText = @"INSERT INTO [dbo].[InformationLog]
				([EventId], [Date], [Level], [Logger], [Message], [MachineName])
				VALUES
				(@EventId, @Date,@Level, @Logger, @Message, @MachineName)",

            };
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@EventId", new NLog.Layouts.SimpleLayout("${event-properties:EventName}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Date", new NLog.Layouts.SimpleLayout("${longdate}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Level", new NLog.Layouts.SimpleLayout("${level}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Logger", new NLog.Layouts.SimpleLayout("${logger}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@Message", new NLog.Layouts.SimpleLayout("${message}")));
            dbTarget.Parameters.Add(new DatabaseParameterInfo("@MachineName", new NLog.Layouts.SimpleLayout("${machinename}")));

            return dbTarget;
        }
    }
}
