﻿using System.Text.Json.Serialization;

using Microsoft.AspNetCore.Mvc;

namespace API_Bootstrap.Configurations
{
    public static class JsonConfiguration
    {
        public static JsonOptions AddApiJsonConfiguration(this JsonOptions options)
        {
            options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            return options;
        }
    }
}
