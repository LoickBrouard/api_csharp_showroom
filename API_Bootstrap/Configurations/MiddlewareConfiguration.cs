﻿using API_Bootstrap.Exceptions;

namespace API_Bootstrap.Configurations
{
    public static class MiddlewareConfiguration
    {
        public static IServiceCollection AdExceptionHandlerMiddleware(this IServiceCollection services)
        {
            return services.AddTransient<ExceptionHandlerMiddleware>();
        }
    }
}
