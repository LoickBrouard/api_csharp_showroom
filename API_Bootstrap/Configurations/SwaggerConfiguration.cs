﻿using System.Reflection;

using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace API_Bootstrap.Configurations
{

    public static class SwaggerConfiguration
    {
        public static SwaggerGenOptions ApiSwaggerOptions(this SwaggerGenOptions options)
        {
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.SchemaFilter<EnumSchemaFilter>();
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            return options;
        }
    }

    public class EnumSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (context.Type.IsEnum)
            {
                model.Enum.Clear();

                var names = Enum.GetNames(context.Type).ToList();

                names.ForEach(name => model.Enum.Add(new OpenApiString($"{name}", false)));


                // the missing piece that will make sure that the new schema will not replace the mock value with a wrong value 
                // this is the default behavior - the first possible enum value as a default "example" value
                model.Example = new OpenApiString(names.First());
            }
        }
    }
}
