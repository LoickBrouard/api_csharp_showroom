﻿using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Common.Models;

namespace InputAndDispatcher.Handlers.Test.Models
{
    internal class SimpleQuery : IQuery
    {
        public InputValidationResult Validate()
            => new InputValidationBuilder().Build();
    }

    internal class SimpleQueryHandler : IQueryHandler<SimpleQuery, string>
    {
        public string Handle(SimpleQuery query)
            => "handled";
    }
}
