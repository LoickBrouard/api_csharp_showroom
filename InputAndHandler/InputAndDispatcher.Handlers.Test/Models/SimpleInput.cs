﻿using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Common.Models;

namespace InputAndDispatcher.Handlers.Test.Models
{
    internal class SimpleInput : IInput
    {
        public InputValidationResult Validate() 
            => new InputValidationBuilder().Build();
    }
}
