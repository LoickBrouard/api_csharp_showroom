﻿using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Common.Models;

namespace InputAndDispatcher.Handlers.Test.Models
{
    internal class SimpleCommand : ICommand
    {
        public InputValidationResult Validate()
            => new InputValidationBuilder().Build();
    }

    internal class SimpleCommandHandler : ICommandHandler<SimpleCommand, bool>
    {
        public bool Handle(SimpleCommand command)
            => true;
    }
}
