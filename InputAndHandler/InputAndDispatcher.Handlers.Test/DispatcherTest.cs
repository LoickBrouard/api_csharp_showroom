using System;

using InputAndDispatcher.Common.Exceptions;
using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Handlers.Test.Helpers;
using InputAndDispatcher.Handlers.Test.Models;

using Xunit;

namespace InputAndDispatcher.Handlers.Test
{
    public class DispatcherTest
    {
        private readonly IQueryDispatcher _queryDispatcher;
        public DispatcherTest()
        {
            IServiceProvider serviceProvider = ServiceProviderTest.CreateServiceProviderForTest();
            _queryDispatcher = new QueryDispatcher(serviceProvider);
        }

        [Fact]
        public void QueryReceivedAndDispactcherRegistered_QueryHandled()
        {
            // Arrange
            SimpleQuery simpleQuery = new ();
            
            // Act
            string result = _queryDispatcher.Dispatch<SimpleQuery, string>(simpleQuery);

            // Assert
            Assert.True(result == "handled");
        }

        [Fact]
        public void QueryReceivedAndQueryDispactcherNotRegistered_ThrowNotImplementedExceptionError()
        {
            // Arrange
            SimpleQuery simpleQuery = new();

            // Act
            Action actionToTest = new (() => { bool result = _queryDispatcher.Dispatch<SimpleQuery, bool>(simpleQuery); });
            
            // Assert
            Assert.Throws<DispatcherNotFoundException>(actionToTest);
        }
    }
}