﻿using System;

using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Handlers.Test.Models;

using Microsoft.Extensions.DependencyInjection;

namespace InputAndDispatcher.Handlers.Test.Helpers
{
    internal class ServiceProviderTest
    {
        internal static IServiceProvider CreateServiceProviderForTest()
        {
            ServiceCollection serviceCollection = new ();

            // Add any DI stuff here:
            serviceCollection.AddScoped<IQueryHandler<SimpleQuery, string>, SimpleQueryHandler>();

            // Create the ServiceProvider
            return serviceCollection.BuildServiceProvider();
        }

    }
}
