﻿namespace InputAndDispatcher.Common.Interfaces
{
    public interface ICommandHandler<in TCommand, TCommandResult> where TCommand : ICommand
    {
        TCommandResult Handle(TCommand command);
    }
}
