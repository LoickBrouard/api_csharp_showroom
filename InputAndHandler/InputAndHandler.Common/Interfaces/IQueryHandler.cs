﻿namespace InputAndDispatcher.Common.Interfaces
{
    public interface IQueryHandler<in TQuery, TQueryResult> where TQuery : IQuery
    {
        TQueryResult Handle(TQuery query);
    }
}
