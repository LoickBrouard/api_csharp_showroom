﻿using InputAndDispatcher.Common.Models;

namespace InputAndDispatcher.Common.Interfaces
{
    public interface IInput
    {
        InputValidationResult Validate();
    }
}
