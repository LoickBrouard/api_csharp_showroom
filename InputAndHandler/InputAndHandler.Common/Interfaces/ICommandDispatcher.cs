﻿namespace InputAndDispatcher.Common.Interfaces
{
    public interface ICommandDispatcher
    {
        TCommandResult Dispatch<TCommand, TCommandResult>(TCommand command) where TCommand : ICommand;
    }
}
