﻿using System;

namespace InputAndDispatcher.Common.Exceptions
{

	[Serializable]
	public class DispatcherNotFoundException : Exception
	{
		public DispatcherNotFoundException() { }
		protected DispatcherNotFoundException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

}
