﻿using System.Collections.Generic;

namespace InputAndDispatcher.Common.Models
{
    public class InputValidationBuilder
    {
        private ISet<string> errorMessages { get; set; }

        public InputValidationBuilder()
        {
            errorMessages = new HashSet<string>();
        }

        /// <summary>
        /// Register an error to the validation result
        /// </summary>
        /// <param name="errorMessage"></param>
        public void AddError(string errorMessage)
            => errorMessages.Add(errorMessage);

        /// <summary>
        /// Return a result with the Set of error message if Success is false
        /// </summary>
        /// <returns></returns>
        public InputValidationResult Build()
        {
            if (errorMessages.Count == 0)
                return new InputValidationResult(true);

            return new InputValidationResult(false, errorMessages);
        }
    }
}

