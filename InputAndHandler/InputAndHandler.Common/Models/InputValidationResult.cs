﻿using System.Collections.Generic;

namespace InputAndDispatcher.Common.Models
{
    public class InputValidationResult
    {
        public bool IsSuccess { get; }
        public bool IsFailure { get; }

#if NETCOREAPP2_1_OR_GREATER
        public IEnumerable<string> ErrorMessages { get; }
#else
        public IEnumerable<string> ErrorMessages { get; }
#endif

#if NETCOREAPP2_1_OR_GREATER
        internal InputValidationResult(bool success, ISet<string> errorMessage = null)
#else
        internal InputValidationResult(bool success, ISet<string> errorMessage = null)
#endif
        {
            IsSuccess = success;
            IsFailure = !success;
            ErrorMessages = errorMessage;
        }
    }
}
