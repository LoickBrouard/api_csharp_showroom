﻿using InputAndDispatcher.Common.Interfaces;

using Microsoft.Extensions.DependencyInjection;

namespace InputAndDispatcher.Handlers
{
    public static class ServiceRegistration
    {
        /// <summary>
        /// To register the queries and commands dispatcher
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddDispatchers(this IServiceCollection services)
        {
            return services
                .AddScoped<IQueryDispatcher, QueryDispatcher>()
                .AddScoped<ICommandDispatcher, CommandDispatcher>();
        }
    }
}
