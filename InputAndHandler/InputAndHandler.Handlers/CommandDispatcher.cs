﻿using System;

using InputAndDispatcher.Common.Exceptions;
using InputAndDispatcher.Common.Interfaces;

using Microsoft.Extensions.DependencyInjection;

namespace InputAndDispatcher.Handlers
{
    public partial class CommandDispatcher : ICommandDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public CommandDispatcher(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

        /// <summary>
        /// Send the command to the dispatcher that implement the <see href="ICommandHandler&lt;TCommand,TCommandResult&gt;"/> interface
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <typeparam name="TCommandResult"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="DispatcherNotFoundException"></exception>
        public TCommandResult Dispatch<TCommand, TCommandResult>(TCommand command) where TCommand : ICommand
        {
            try
            {
                var handler = _serviceProvider.GetRequiredService<ICommandHandler<TCommand, TCommandResult>>();
                
                return handler.Handle(command);
            }
            catch (InvalidOperationException)
            {
               throw new DispatcherNotFoundException();
            }
        }
    }
}
