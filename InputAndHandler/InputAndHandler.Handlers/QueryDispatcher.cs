﻿using System;

using InputAndDispatcher.Common.Exceptions;
using InputAndDispatcher.Common.Interfaces;

using Microsoft.Extensions.DependencyInjection;

namespace InputAndDispatcher.Handlers
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public QueryDispatcher(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

        /// <summary>
        /// Send the query to the dispatcher that implement the <see href="IQueryHandler&lt;TQuery,TQueryResult&gt;"/> interface
        /// </summary>
        /// <typeparam name="TQuery"></typeparam>
        /// <typeparam name="TQueryResult"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        /// <exception cref="DispatcherNotFoundException"></exception>
        public TQueryResult Dispatch<TQuery, TQueryResult>(TQuery query) where TQuery : IQuery
        {
            try
            {
                var handler = _serviceProvider.GetRequiredService<IQueryHandler<TQuery, TQueryResult>>();
                return handler.Handle(query);
            }
            catch (InvalidOperationException)
            {
                throw new DispatcherNotFoundException();
            }
        }
    }
}
