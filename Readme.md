
# API Bootstrap (showroom)

An API template showcasing some handy features (global exception handling, NLog configuration, input helpers, dispatcher)


## Authors

- [@lbrouard](https://gitlab.com/LoickBrouard)


## Installation

Clone the project and run !
On build, you can extract InputAndHandler *.nuGet from InputAndHandler/NugetGenerated
    
## Roadmap

* To finish dispatcher tests
* To split Input Validation element as "nuGetable" project
## Features

- Global handling exceptions
- Secure exceptions messages mechanism
- Input validation builder
- Dispatch mechanism
- Query / Command differenciation
- .net standard 2 compatible


## Usage/Examples

### Input example

Implementing ICommand or IQuery : 
* Force to implement a Validate() method to be called by the client and/or the controller
* Allow to benefit to the Dispatch mechnism

```csharp
public class DummyCommand : ICommand
{
    public string Requester { get; set; }

    private string comment;
    public string Comment { get => comment; set => comment = value.TreatInput(); }

    public InputValidationResult Validate()
    {
        InputValidationBuilder inputValidationBuilder = new InputValidationBuilder();

        if (string.IsNullOrWhiteSpace(Requester))
            inputValidationBuilder.AddError($"Argument {nameof(Requester)} is missing");
        if (string.IsNullOrWhiteSpace(Comment))
            inputValidationBuilder.AddError($"Argument {nameof(Comment)} is missing");

        return inputValidationBuilder.Build();
    }
}
```

### Input handler exemple

Implementing the interface ICommandHandler<DummyCommand, int> :
* Force to implement the Handle method (method hadling the input)
* Allow to use the dispatcher mechanism

```csharp
public sealed class DummyCommandHandler : ICommandHandler<DummyCommand, int>
{
    public DummyCommandHandler()
    {
        // DI and init
    }

    /// <inheritdoc/>
    public int Handle(DummyCommand command)
    {
        // Do interresting thing
        return 0;
    }
}
```

__Do not forget to register the handler, in the ServiceConfiguration.cs file__

```csharp
public static IServiceCollection AddApiHandlers(this IServiceCollection services)
{
    return services
        .AddScoped<ICommandHandler<DummyCommand, int>, DummyCommandHandler>()
        .AddScoped<IQueryHandler<DummyQuery, int?>, DummyQuerryHandler>();
}
```

### Controller exemple

In the controller we can use :
* The input validation mechanism
* The Dispatcher allow us to dispatch the input to its handler.

```csharp
public ActionResult<int> AssociatePanelAndLock(DummyCommand command)
{
    if (command.Equals(null))
        return BadRequest("No parameters has been sent to the API");

    InputValidationResult validation = command.Validate();

    if (validation.IsFailure)
        return BadRequest(string.Join(",", validation.ErrorMessages));
    try
    {    
        return Ok(_commandDispatcher.Dispatch<DummyCommand, int>(command));
    }
    catch(SpecificException ex)
    {
        // Spefic treatment, redirection, ...
        
        // Throw exception that can be presented to the client while loging the inner     exception
        throw new SafeApiException("Oups, something went wrong, please retry in 5 minutes", ex);
    }
}

```
