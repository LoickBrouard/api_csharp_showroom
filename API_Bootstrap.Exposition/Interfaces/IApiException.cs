﻿using System;
using System.Net;

namespace API_Bootstrap.Exposition.Interfaces
{
    public interface IApiException
    {
        HttpStatusCode StatusCode { get; }
        Guid Id { get; }
    }

    /// <summary>
    /// Exceptions dont le message d'erreur peut être présenté à l'utilisateur final
    /// </summary>
    public interface ISafeException
    {
        string SafeErrorMessage { get; }
    }

    /// <summary>
    /// Exceptions qui décrivent un plantage irrécupérable de l’application ou du système,
    /// ou une défaillance catastrophique nécessitant une attention immédiate.
    /// </summary>
    public interface ICriticalException
    {
    }

    /// <summary>
    /// Exceptions qui indiquent quand le flux actuel de l’exécution s’est arrêté en raison d’une erreur. 
    /// Elles doivent indiquer une erreur dans l’activité en cours, et non une défaillance de l’application.
    /// https://learn.microsoft.com/fr-fr/dotnet/api/microsoft.extensions.logging.loglevel?view=dotnet-plat-ext-6.0
    /// </summary>
    public interface IErrorException
    {
    }

    /// <summary>
    /// Exceptions qui mettent en évidence un événement anormal ou inattendu dans le flux de l’application,
    /// mais qui ne provoque pas l’arrêt de l’exécution de l’application.
    /// https://learn.microsoft.com/fr-fr/dotnet/api/microsoft.extensions.logging.loglevel?view=dotnet-plat-ext-6.0
    /// </summary>
    public interface IWarningException
    {
    }

    /// <summary>
    /// Exceptions utilisées pour suivre le flux général de l’application.
    /// Elles ont généralement une utilité à long terme.
    /// https://learn.microsoft.com/fr-fr/dotnet/api/microsoft.extensions.logging.loglevel?view=dotnet-plat-ext-6.0
    /// </summary>
    public interface IInformationsException
    {
    }

}
