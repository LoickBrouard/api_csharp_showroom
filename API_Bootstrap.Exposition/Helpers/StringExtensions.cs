﻿namespace API_Bootstrap.Exposition.Helpers
{
    internal static class StringExtensions
    {
        /// <summary>
        /// Remove leading and trailling white spaces and remove all occurences of \r \n caracters
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string TreatInput(this string input)
        {
            return input
                .Trim()
                .Replace("\r", "")
                .Replace("\n", "");
        }
    }
}
