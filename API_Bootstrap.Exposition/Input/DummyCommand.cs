﻿using API_Bootstrap.Exposition.Helpers;

using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Common.Models;

namespace API_Bootstrap.Exposition.Input
{
    public class DummyCommand : ICommand
    {
        private string login;
        private string comment;

        public string Requester { get => login; set => login = value.TreatInput(); }
        public string Comment { get => comment; set => comment = value.TreatInput(); }

        public InputValidationResult Validate()
        {
            InputValidationBuilder inputValidationBuilder = new InputValidationBuilder();

            if (string.IsNullOrWhiteSpace(Requester))
                inputValidationBuilder.AddError($"Argument {nameof(Requester)} is missing");
            if (string.IsNullOrWhiteSpace(Comment))
                inputValidationBuilder.AddError($"Argument {nameof(Comment)} is missing");

            return inputValidationBuilder.Build();
        }
    }
}
