﻿using API_Bootstrap.Exposition.Helpers;

using InputAndDispatcher.Common.Interfaces;
using InputAndDispatcher.Common.Models;

namespace API_Bootstrap.Exposition.Input
{
    public class DummyQuery : IQuery
    {
        private string id;
        private string name;

        public string RefProduct { get => id; set => id = value.TreatInput(); }
        public string VehicleCode { get => name; set => name = value.TreatInput(); }

        public InputValidationResult Validate()
        {
            InputValidationBuilder inputValidationBuilder = new InputValidationBuilder();

            if (string.IsNullOrWhiteSpace(RefProduct))
                inputValidationBuilder.AddError($"Argument {nameof(RefProduct)} is missing");
            if (string.IsNullOrWhiteSpace(VehicleCode))
                inputValidationBuilder.AddError($"Argument {nameof(VehicleCode)} is missing");

            return inputValidationBuilder.Build();
        }
    }
}
