﻿using System.Collections.Generic;

namespace API_Bootstrap.Exposition.Output
{
    public class DummyOutput
    {
        public string ResultValue { get; set; }
        public ICollection<string> ErrorMessage { get; }

        public DummyOutput()
        {
            ErrorMessage = new List<string>();
        }

        public void AddError(string message)
        {
            ErrorMessage.Add(message);
        }
    }
}
