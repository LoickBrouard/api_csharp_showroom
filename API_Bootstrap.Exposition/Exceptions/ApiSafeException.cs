﻿using System;
using System.Net;

using API_Bootstrap.Exposition.Interfaces;

namespace API_Bootstrap.Exposition.Exceptions
{
    ///<inheritdoc cref="ISafeException"/>
    public class ApiSafeException : Exception, ISafeException, IApiException, IErrorException
    {
        public ApiSafeException(string message) : base(message) { }
        public ApiSafeException(string message, Exception innerException) : base(message, innerException) { }

        public virtual HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
        public virtual string SafeErrorMessage => Message;

        public Guid Id => new Guid();
    }
}
